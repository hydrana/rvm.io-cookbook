# rvm_io

Installs RVM, from https://rvm.io/, and installs a Ruby for users.

This cookbook is inspiring from [NikolayMurha's chef-rvm](https://github.com/NikolayMurha/chef-rvm).

## Requirements

### Platforms

* Ubuntu 20.04 LTS

### Chef

* Chef >= 16

## Attributes

This cookbook supports 2 ways to install RVM:
 * System wide: Installed rubies are available to all users.
 * Single user: Installed rubies are available only to the given user.

Please, read the [rvm.io install page](https://rvm.io/rvm/install) in order
to be aware of the different information like never use the `root` account,
the umask security risk with the system wide installtion and more!

### RVM Version (optional)

To install specific RVM Version

```ruby
node['rvm_io'] = {
  rvm_version: "1.29.3"
}
```

### Single user

To install RMV and Rubies for a specific user:

```ruby
node['rvm_io'] = {
  users: {
    ubuntu: {
      rubies: {
        '2.7.2' => 'install'
      }
    }
  }
}
```

This will installs Ruby 2.7.2 as user `ubuntu`.

### System wide

To install RMV and Rubies for all users:

```ruby
node['rvm_io'] = {
  rubies: {
    '2.7.2' => 'install'
  }
}
```

This will installs Ruby 2.7.2 for all users.

Last but not least, you could like to add user(s) to the `rvm` group, allowing
them to update RVM, rubies and gemsets, which you can do by adding a `for` key
in the `rubies` one like so:

```ruby
node['rvm_io'] = {
  rubies: {
    for: [
      'hydrana'
    ],
    '2.7.2' => 'install'
  }
}
```

This will adds the `hydrana` user to the `rvm` group -- or the other way around
depending on your point of view.

## .gemrc

This cookbook also allows you to configure the `.gemrc` file per users or system
wide.

### Single user

```ruby
node['rvm_io'] = {
  users: {
    ubuntu: {
      gemrc: {
        gem: --no-document
      }
    }
  }
}
```

### System wide

```ruby
node['rvm_io'] = {
  gemrc: {
    gem: --no-document
  }
}
```

## Recipes

```ruby
recipe[rvm_io::default] # Full installations
recipe[rvm_io::gemrc] # Write down the .gemrc file
recipe[rvm_io::install_rvm] # GPG keys and RVM installation
recipe[rvm_io::packages] # Required packages
recipe[rvm_io::rubies] # Installs Rubies for each given users
recipe[rvm_io::rvm] # Triggers packages and then install_rvm
```

## Author

Hydrana SAS
