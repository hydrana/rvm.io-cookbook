# frozen_string_literal: true

# Chef InSpec test for recipe rvm_io::default

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe command("sudo --user hydrana bash -c 'source /home/hydrana/.rvm/scripts/rvm; rvm list'") do
  its('exit_status') { should eq 0 }
  its('stdout') { should match(/ruby-2\.7\.2/) }
end
