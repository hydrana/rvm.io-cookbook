# frozen_string_literal: true

# Chef InSpec test for recipe rvm_io::default

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe command("bash -c 'source /usr/local/rvm/scripts/rvm; rvm version'") do
  its('exit_status') { should eq 0 }
  its('stdout') { should match(/1.29.3/) }
end
