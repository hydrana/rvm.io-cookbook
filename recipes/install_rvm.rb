# frozen_string_literal: true

#
# Cookbook:: rvm_io
# Recipe:: default
#
# Copyright:: 2022, The Authors, All Rights Reserved.
rvm_version = if node['rvm_io'].key?('rvm_version')
                "-- --version #{node['rvm_io']['rvm_version']}"
              else
                'stable'
              end

#
# Per user RVM install
#

# rubocop:disable Metrics/BlockLength
node['rvm_io']['users'].each do |username, _|
  execute "import mpapis GPG key as #{username}" do
    command 'curl -sSL https://rvm.io/mpapis.asc | gpg --import -'
    login true
    user username

    only_if do
      `sudo --user #{username} bash -c 'gpg --list-keys | grep 409B6B1796C275462A1703113804BB82D39DC0E3 | wc -l'` == "0\n"
    end
  end
  execute "trust mpapis GPG key as #{username}" do
    command 'echo 409B6B1796C275462A1703113804BB82D39DC0E3:6: | gpg --import-ownertrust'
    login true
    user username
  end

  execute "import mpapis GPG key as #{username}" do
    command 'curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -'
    login true
    user username

    only_if do
      `sudo --user #{username} bash -c 'gpg --list-keys | grep 7D2BAF1CF37B13E2069D6956105BD0E739499BDB | wc -l'` == "0\n"
    end
  end
  execute "trust mpapis GPG key as #{username}" do
    command 'echo 7D2BAF1CF37B13E2069D6956105BD0E739499BDB:6: | gpg --import-ownertrust'
    login true
    user username
  end

  execute "installs RVM for #{username} user" do
    command "curl -sSL https://get.rvm.io | bash -s #{rvm_version}"
    login true
    user username

    not_if { ::File.exist?("#{Etc.getpwnam(username).dir}/.rvm/scripts/rvm") }
  end
end
# rubocop:enable Metrics/BlockLength

#
# System wide RVM install
#

if node['rvm_io'].key?('rubies') || node['rvm_io'].key?('gemrc')
  execute 'import mpapis GPG key system wide' do
    command 'curl -sSL https://rvm.io/mpapis.asc | gpg --import -'
    login true
    user 'root'

    only_if do
      `gpg --list-keys | grep 409B6B1796C275462A1703113804BB82D39DC0E3 | wc -l` == "0\n"
    end
  end
  execute 'trust mpapis GPG key system wide' do
    command 'echo 409B6B1796C275462A1703113804BB82D39DC0E3:6: | gpg --import-ownertrust'
    login true
    user 'root'
  end

  execute 'import mpapis GPG key system wide' do
    command 'curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -'
    login true
    user 'root'

    only_if do
      `gpg --list-keys | grep 7D2BAF1CF37B13E2069D6956105BD0E739499BDB | wc -l` == "0\n"
    end
  end
  execute 'trust mpapis GPG key system wide' do
    command 'echo 7D2BAF1CF37B13E2069D6956105BD0E739499BDB:6: | gpg --import-ownertrust'
    login true
    user 'root'
  end

  execute 'installs RVM system wide' do
    command "curl -sSL https://get.rvm.io | sudo bash -s #{rvm_version}"
    login true
    user 'root'

    not_if { ::File.exist?('/usr/local/rvm/scripts/rvm') }
  end

  if node['rvm_io'].key?('rubies') && node['rvm_io']['rubies'].key?('for')
    group 'rvm' do
      action :modify
      append true
      members node['rvm_io']['rubies']['for']
    end
  end
end
