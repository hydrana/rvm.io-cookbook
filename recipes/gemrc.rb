# frozen_string_literal: true

#
# Cookbook:: rvm_io
# Recipe:: default
#
# Copyright:: 2022, The Authors, All Rights Reserved.

#
# Per user RVM install
#

node['rvm_io']['users'].each do |username, rvm|
  next unless rvm.key?('gemrc')

  gemrc = rvm['gemrc'].reduce('') do |acc, (key, value)|
    acc += "#{key}: #{value}"
    acc
  end

  file File.join(Etc.getpwnam(username).dir, '.gemrc') do
    content gemrc
  end
end

#
# System wide RVM install
#
if node['rvm_io'].key?('gemrc')
  gemrc = node['rvm_io']['gemrc'].reduce('') do |acc, (key, value)|
    acc += "#{key}: #{value}"
    acc
  end

  file File.join('/root', '.gemrc') do
    content gemrc
  end
end
