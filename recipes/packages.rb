# frozen_string_literal: true

#
# Cookbook:: rvm_io
# Recipe:: default
#
# Copyright:: 2022, The Authors, All Rights Reserved.

include_recipe 'apt'

node['rvm_io']['packages'].each do |name|
  package name
end
