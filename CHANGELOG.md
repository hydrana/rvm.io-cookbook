# rvm_io CHANGELOG

This file is used to list changes made in each version of the rvm_io cookbook.

## Unreleased

Allows installing a specific RVM version (by [dschaerli](https://gitlab.com/dschaerli))

## 0.2.2

Fixes system wide rvm installation guard

## 0.2.1

Allows adding user to the `rvm` groups

## 0.2.0

Adds support for Single user and System wide installation

## 0.1.1

Adds issues_url and source_url

## 0.1.0

Initial release.
